import itertools

try:
    from scapy.all import rdpcap, wrpcap, IP, TCP
except ImportError:
    print("first do 'pip install scapy'")
    exit(-1)


class MT19937:
    w, n = 32, 624
    f = 1812433253
    m, r = 397, 31
    a = 0x9908B0DF
    d, b, c = 0xFFFFFFFF, 0x9D2C5680, 0xEFC60000
    u, s, t, l = 11, 7, 15, 18

    def __init__(self, seed):
        self.X = [0] * MT19937.n
        self.cnt = 0
        self.initialize(seed)

    def initialize(self, seed):
        self.X[0] = seed
        for i in range(1, MT19937.n):
            self.X[i] = (MT19937.f * (self.X[i - 1] ^ (self.X[i - 1] >> (MT19937.w - 2))) + i) & ((1 << MT19937.w) - 1)
        self.twist()

    def twist(self):
        for i in range(MT19937.n):
            lower_mask = (1 << MT19937.r) - 1
            upper_mask =  (~lower_mask) & ((1 << MT19937.w) - 1)
            tmp = (self.X[i] & upper_mask) + (self.X[(i + 1) % MT19937.n] & lower_mask)
            tmpA = tmp >> 1
            if (tmp % 2):
                tmpA = tmpA ^ MT19937.a
            self.X[i] = self.X[(i + MT19937.m) % MT19937.n] ^ tmpA
        self.cnt = 0

    def temper(self):
        if self.cnt == MT19937.n:
            self.twist()
        y = self.X[self.cnt]
        y = y ^ ((y >> MT19937.u) & MT19937.d)
        y = y ^ ((y << MT19937.s) & MT19937.b)
        y = y ^ ((y << MT19937.t) & MT19937.c)
        y = y ^ (y >> MT19937.l)
        self.cnt += 1
        return y & ((1 << MT19937.w) - 1)

    def stream(self):
        while True:
            current = self.temper()
            yield from current.to_bytes(length=4, byteorder='little')


class BadDiffieHellman(object):
    g = 0x02
    p = int.from_bytes(bytearray([
		0xEE, 0x38, 0x6B, 0xFB, 0x5A, 0x89, 0x9F, 0xA5, 0xAE,
		0x9F, 0x24, 0x11, 0x7C, 0x4B, 0x1F, 0xE6, 0x49, 0x28,
		0x66, 0x51, 0xEC, 0xE6, 0x53, 0x81, 0xFF, 0xFF, 0xFF,
		0xFF, 0xFF, 0xFF, 0xFF, 0xFF
    ]), byteorder='little')

    def __init__(self, timestamp: int):
        self.timestamp = timestamp

    def random_int(self, seed, bytes_length = 32) -> int:
        r = MT19937(seed)
        buffer = bytes(itertools.islice(r.stream(), bytes_length))
        return int.from_bytes(buffer, byteorder='little')

    def client_gen(self) -> tuple[int, int]:
        a = self.random_int(self.timestamp)
        return a, pow(self.g, a, self.p)

    def server_gen(self) -> tuple[int, int]:
        b = self.random_int(self.timestamp + 1)
        return b, pow(self.g, b, self.p)

    def client_exchange(self, a, B) -> int:
        return pow(B, a, self.p)

    def server_exchange(self, b, A) -> int:
        return pow(A, b, self.p)

    @staticmethod
    def generate_sk(timestamp: int):
        dh = BadDiffieHellman(timestamp)
        a, A = dh.client_gen()
        b, B = dh.server_gen()
        print('a=', hex(a), '\nA=', hex(A), '\n\n')
        print('b=', hex(b), '\nB=', hex(B), '\n\n')

        assert a != b
        assert A != B

        client_sk = dh.client_exchange(a, B)
        server_sk = dh.server_exchange(b, A)
        assert client_sk == server_sk
        
        return client_sk


class Encryptor(object):
    def __init__(self, timestamp: int):
        self.sk: int = BadDiffieHellman.generate_sk(timestamp)
        bytes = self.sk.bit_length() // 8 + 1
        self.sk_bytes: bytes = self.sk.to_bytes(bytes, byteorder='little')
        print('SK', self.sk_bytes.hex())

    def __call__(self, data: bytes):
        sk_gen = itertools.cycle(self.sk_bytes)
        result = bytearray()
        
        for i, (x, k) in enumerate(zip(data, sk_gen)):
            y = x ^ k ^ (i & 0xFF)
            result.append(y)
        
        return result 
    

def patch_pcap(
        input_filepath: str,
        output_filepath: str,
        seed: int,
        src_pattern: str,
        dst_pattern: str):

    enc = Encryptor(seed)

    b_old_flag = src_pattern.encode('utf-16le')
    b_new_flag = dst_pattern.encode('utf-16le')

    print('fixing patch length...')
    diff = len(b_new_flag) - len(b_old_flag)
    if diff > 0:
        b_old_flag += b'\x00' * diff
    else:
        b_new_flag += b'\x00' * diff

    assert len(b_old_flag) == len(b_new_flag)

    data = rdpcap(input_filepath)
    for packet in data:
        if TCP not in packet:
            continue
        
        content = enc(bytes(packet[TCP].payload))
        if b_old_flag in content:
            print('found packet...')
            print(packet.time,
                  packet[IP].src, packet[IP].dst,
                  packet[TCP].sport, packet[TCP].dport)
            print('replacing...')
            content = content.replace(b_old_flag, b_new_flag)
            content = bytes(enc(content))
            print('fixing packet...')
            packet[TCP].payload.load = content
            break

    wrpcap(output_filepath, data)


def verify_pcap(filepath: str, seed: int, pattern: str):
    enc = Encryptor(seed)

    b_pattern = pattern.encode('utf-16le')
    data = rdpcap(filepath)
    for packet in data:
        if TCP not in packet:
            continue
        
        content = enc(bytes(packet[TCP].payload))
        if b_pattern in content:
            print('found packet with pattern!')
            print(packet.time,
                  packet[IP].src, packet[IP].dst,
                  packet[TCP].sport, packet[TCP].dport)
            break


if __name__ == '__main__':
    """
    seed = 1666749350 - 1
    # 192.168.43.115 - server, 192.168.43.183 - client

    b = bb 49 82 ed 7e fc 07 95 a8 b3 da 5c 76 3c 0b 63 ab 84 73 91 27 16 fc 5d c3 ea 39 87 e1 e3 49 fa
    B = 6a df aa 5d d8 2e c7 7c d8 27 e3 97 ef 8e 14 91 e6 47 59 cd c1 73 45 f5 8d 34 d8 f6 39 d1 e9 7a
    A = 2a d3 d7 f3 08 5e a3 56 09 75 7b 8a ec 41 b9 05 6f a0 8f ef 49 8c 60 a8 4a 09 cb 15 fa 22 12 06

    SK = 02 c9 98 98 5a d9 e4 5c 2f 7b f9 ea 8d 07 c6 04 df 90 b5 14 0c e5 e7 37 74 3d 20 89 58 bb 96 57
    """

    pcap_filepath = 'capture.pcapng'
    pcap_o_filepath = 'capture_o.pcapng'
    seed = 1666749350 - 1
    old_flag = 'MCTF{REPLACE_FLAG}'
    new_flag = 'MCTF{OwYouLookSoSecute}'
    patch_pcap(
        input_filepath=pcap_filepath,
        output_filepath=pcap_o_filepath,
        seed=seed,
        src_pattern=old_flag,
        dst_pattern=new_flag)

    verify_pcap(pcap_o_filepath, seed=seed, pattern=new_flag)
