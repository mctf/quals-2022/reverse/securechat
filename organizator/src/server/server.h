#pragma once
#define _AFXDLL
#include "Definition.h"

class Server
{
public:
	Server();
	~Server();
	bool isConnected();
	void init();
	void clientThread(LPVOID lParam);
	void exchange(Client* client);
	void loop();
	int send(Client * client, Message * message);
	int recv(Client* client);
	void setHWND(HWND hwnd);
	bool authenticate(const Client* client, const std::wstring & username, const std::wstring& password);
	void sendMessageGroup(Message * message);
private:
	bool _isConnected;
	int _serverPort;
	std::list<Client*> _clientList;
	SOCKET _socClient;
	SOCKET _socListenClient;
	HWND _hwnd;
};

