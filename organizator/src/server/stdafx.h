#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <iostream>
#include <list>
#include <string>
#include <thread>
#include <fstream>
#include <vector>
#include <random>
#include <conio.h> 
#include <functional>
#include <windows.h>
#include <tchar.h>
#include <winsock2.h>
#include "commctrl.h"
#include <objidl.h>
#include <gdiplus.h>
using namespace Gdiplus;
#pragma comment (lib,"Gdiplus.lib")
#pragma comment (lib,"WS2_32.lib")
#pragma comment (lib,"Comctl32.lib")
