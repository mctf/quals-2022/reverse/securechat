#include "stdafx.h"
#include "Server.h"
#include "Resource.h"
#include "Definition.h"
#include "DH.h"


#define TARGET_USERNAME L"admin"
#define TARGET_PASSWORD L"password"


Server::Server(): _isConnected(false) {

}


void Server::init() {
	std::cout << "Starting up server" << std::endl;

	WSADATA wsaData;

	sockaddr_in local;
	local.sin_family = AF_INET;
	local.sin_addr.s_addr = INADDR_ANY;
	local.sin_port = htons((u_short)8084);

	if (WSAStartup(0x101, &wsaData) != 0)
		return;

	_socListenClient = socket(AF_INET, SOCK_STREAM, 0);
	if (_socListenClient == INVALID_SOCKET)
		return;

	if (bind(_socListenClient, (sockaddr*)&local, sizeof(local)) != 0) {
		auto status = WSAGetLastError();
		return;
	}

	if (listen(_socListenClient, 10) != 0)
		return;

	_isConnected = true;
	return;
}

void Server::exchange(Client * client) {
	const size_t PModuleLength = 32;
	unsigned long cryptoGModule = 0x02;

	unsigned char bufferP[PModuleLength] = {
		0xEE, 0x38, 0x6B, 0xFB, 0x5A, 0x89, 0x9F, 0xA5, 0xAE,
		0x9F, 0x24, 0x11, 0x7C, 0x4B, 0x1F, 0xE6, 0x49, 0x28,
		0x66, 0x51, 0xEC, 0xE6, 0x53, 0x81, 0xFF, 0xFF, 0xFF,
		0xFF, 0xFF, 0xFF, 0xFF, 0xFF
	};
	std::vector<char> cryptoPModule(bufferP, bufferP + (sizeof(bufferP) / sizeof(unsigned char)));

	DHExchanger<PModuleLength> exchanger(cryptoPModule, cryptoGModule);

	unsigned char buffer[PModuleLength] = { 0 };
	::recv(client->socket, (char*)buffer, sizeof(buffer), 0);
	std::vector<char> ePK(buffer, buffer + PModuleLength);

	Sleep(1000);

	std::vector<char> PK;
	exchanger.GenerateExchangeData(PK);
	std::copy(PK.begin(), PK.end(), buffer);
	::send(client->socket, (char*)buffer, sizeof(buffer), 0);

	exchanger.CompleteExchangeData(ePK, client->sk);
}


void Server::clientThread(LPVOID lParam) {
	Client* client = (Client*)lParam;

	this->exchange(client);

	while (1) {
		if (this->recv(client))
			break;

		Sleep(200);
	}
}


void Server::loop() {
	sockaddr_in from;
	int fromlen = sizeof(from);

	while (1) {
		_socClient = accept(_socListenClient, (struct sockaddr*)&from, &fromlen);
		if (_socClient != INVALID_SOCKET) {
			std::cout << "Starting up thread" << std::endl;
			Client* client = new Client;
			client->socket = _socClient;
			client->username = std::wstring(L"anonymous");
			
			auto threadFunction = std::bind(&Server::clientThread, this, std::placeholders::_1);
			client->thread = std::thread(threadFunction, client);

			_clientList.push_back(client);
		}

		Sleep(1000);
	}
}

Server::~Server() {
	closesocket(_socListenClient);

	WSACleanup();

	if (_clientList.size() != 0) {
		for (auto client: _clientList) {
			delete client;
		}
	}
}

bool Server::isConnected() {
	return _isConnected;
}


int Server::send(Client* client, Message* message) {
	for (size_t i = 0; i < sizeof(Message); ++i) {
		((unsigned char*)message)[i] ^= client->sk[i % 32] ^ (i & 0xFF);
	}
	if (::send(client->socket, (char*)message, sizeof(Message), 0) == -1) {
		_clientList.remove(client);
		return 1;
	}

	return 0;
}

int Server::recv(Client * client) {
	Message * message = new Message;

	if (::recv(client->socket, (char*)message, sizeof(Message), 0) == -1) {
		std::wcout << L"User " << client->username << L" leave" << std::endl;
		// client->thread.join();
		// delete client;
		return 1;
	}
	for (size_t i = 0; i < sizeof(Message); ++i) {
		((unsigned char*)message)[i] ^= client->sk[i % 32] ^ (i & 0xFF);
	}

	switch (message->code) {
	case MessageType::CHAT: {
		sendMessageGroup(message);
		break;
	}
	case MessageType::LOGIN: {
		std::cout << "Authentication request" << std::endl;
		int result = authenticate(client, message->sender, message->content);
		message->code = result ? MessageType::LOGIN_SUCCESS: MessageType::LOGIN_FAIL;
		ZeroMemory(message->content, sizeof(message->content));

		send(client, message);
		break;
	}
	}

	return 0;
}

void Server::setHWND(HWND hwnd) {
	_hwnd = hwnd;
}

void Server::sendMessageGroup(Message* message) {
	for (auto client : _clientList) {
		std::wcout << std::wstring(message->sender) << L": " << std::wstring(message->content) << std::endl;
		if (client->username != std::wstring(message->sender)) {
			std::wcout << L"Redirect to " << client->username << std::endl;
			Message* copy = new Message;
			memcpy(copy, message, sizeof(Message));
			send(client, copy);
		}
	}
}

bool Server::authenticate(const Client * client, const std::wstring & username, const std::wstring & password) {
	std::ifstream file("passwords.db");
	if (file.is_open()) {
		std::string line;
		while (std::getline(file, line)) {
			const auto offset = line.find(' ');

			std::wstring _username(line.c_str(), line.c_str() + offset);
			std::wstring _password(line.c_str() + offset + 1, line.c_str() + line.length());

			if (username == _username && password == _password) {
				for (auto& other : _clientList) {
					if (other->socket == client->socket) {
						std::wcout << L"User " << username << " has authenticated" << std::endl;
						other->username = username;
						return true;
					}
				}
			}
		}

		file.close();
	}

	return false;
}
