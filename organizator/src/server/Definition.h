#pragma once

#include "stdafx.h"

struct Client {
	SOCKET socket;
	std::wstring username;
	std::thread thread;
	std::vector<char> sk;
};

enum MessageType {
	CHAT = 1,
	END_CHAT,
	LOGIN,
	LOGIN_SUCCESS,
	LOGIN_FAIL
};

struct Message {
	MessageType code;
	WCHAR sender[128];
	WCHAR content[8192];
};
