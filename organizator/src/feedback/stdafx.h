#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_WARNINGS
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "resource.h"
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <conio.h> 
#include <iostream>
#include <vector>
#include <random>
#include <functional>
#include <string>
#include <new>
#include <thread>
#include <winsock2.h>
#include "commctrl.h"
#include <objidl.h>
#include <gdiplus.h>
#include <windowsx.h>
#include <mmsystem.h>
using namespace Gdiplus;
#pragma comment(lib, "Winmm.lib")
#pragma comment (lib,"Gdiplus.lib")
#pragma comment (lib,"WS2_32.lib")
#pragma comment (lib,"Comctl32.lib")
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
