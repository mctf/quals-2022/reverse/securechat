#pragma once

#include "stdafx.h"
#include "resource.h"
#include "Definition.h"


class Connection {
public:
	~Connection();
	static Connection& instance();
	void init(HWND hwnd, const char * ipAddress, int port);
	int send(Message * message);
	int recv();
	void exchange();
	UINT loop(LPVOID lParam);
	bool isConnected();
	void setHwnd(HWND hwnd);
	void setUsername(std::wstring username);
	std::wstring& getUsername();
    Connection(Connection const&) = delete;
    void operator=(Connection const&) = delete;
private:
    Connection() {}

	bool _isConnected;
	SOCKET _connect;
	HWND _hwnd;
    std::thread * _thread;
	std::wstring _username; 
};
