#include "stdafx.h"
#include "connection.h"
#include "DH.h"

std::vector<char> SK;


Connection& Connection::instance() {
	static Connection instance;
	return instance;
}


Connection::~Connection() {
	if (_isConnected)
		closesocket(_connect);
}

void Connection::init(HWND hwnd, const char * ipAddress, int port) {
	// "127.0.0.1", 8084

	struct hostent *hp;
	unsigned int addr;
	struct sockaddr_in server;

	_hwnd = hwnd;
	
	WSADATA wsaData;
	if (WSAStartup(0x101, &wsaData) != 0) {
		return;
	}

	_connect = socket(AF_INET, SOCK_STREAM, 0);
	if (_connect == INVALID_SOCKET)
		return;

	addr = inet_addr(ipAddress);

	hp = gethostbyaddr((char*)&addr, sizeof(addr), AF_INET);
	if (hp == NULL) {
		closesocket(_connect);
		return;
	}

	server.sin_addr.s_addr = *((unsigned long*)hp->h_addr);
	server.sin_family = AF_INET;
	server.sin_port = htons(port);

	if(connect(_connect, (struct sockaddr*)&server, sizeof(server))) {
		closesocket(_connect);
		return;	
	}

	auto threadFunction = std::bind(&Connection::loop, this, std::placeholders::_1);
	_thread = new std::thread(threadFunction, nullptr);
	_isConnected = true;
	return;
}

void Connection::exchange() {
	const size_t PModuleLength = 32;
	unsigned long cryptoGModule = 0x02;

	unsigned char bufferP[PModuleLength] = {
		0xEE, 0x38, 0x6B, 0xFB, 0x5A, 0x89, 0x9F, 0xA5, 0xAE,
		0x9F, 0x24, 0x11, 0x7C, 0x4B, 0x1F, 0xE6, 0x49, 0x28,
		0x66, 0x51, 0xEC, 0xE6, 0x53, 0x81, 0xFF, 0xFF, 0xFF,
		0xFF, 0xFF, 0xFF, 0xFF, 0xFF
	};
	std::vector<char> cryptoPModule(bufferP, bufferP + (sizeof(bufferP) / sizeof(unsigned char)));

	DHExchanger<PModuleLength> exchanger(cryptoPModule, cryptoGModule);
	std::vector<char> PK;
	exchanger.GenerateExchangeData(PK);

	unsigned char buffer[PModuleLength] = { 0 };
	std::copy(PK.begin(), PK.end(), buffer);

	::send(_connect, (char*)buffer, sizeof(buffer), 0);
	::recv(_connect, (char*)buffer, sizeof(buffer), 0);

	std::vector<char> ePK(buffer, buffer + sizeof(buffer));
	exchanger.CompleteExchangeData(ePK, SK);
}

UINT Connection::loop(LPVOID lParam) {
	this->exchange();

	while (1) {
		if (this->recv())
			break;
	}
	return 0;
}

int Connection::send(Message * message) {
	for (size_t i = 0; i < sizeof(Message); ++i) {
		((unsigned char*)message)[i] ^= SK[i % 32] ^ (i & 0xFF);
	}
	return ::send(_connect, (char*)message, sizeof(Message), 0) == -1
		? 1
		: 0;
}

int Connection::recv() {
	Message * message = new Message;

	if (::recv(_connect, (char*)message, sizeof(Message), 0) == -1)
		return 1;

	for (size_t i = 0; i < sizeof(Message); ++i) {
		((unsigned char*)message)[i] ^= SK[i % 32] ^ (i & 0xFF);
	}

	SendMessage(_hwnd, WM_COMMAND, (WPARAM)IDC_RECEIVE, (LPARAM)message);

	return 0;

}

bool Connection::isConnected() {
	return _isConnected;
}

void Connection::setUsername(std::wstring username) {
	_username = username;
}

void Connection::setHwnd(HWND hwnd) {
	_hwnd = hwnd;
}


std::wstring & Connection::getUsername() {
	return _username;
}
