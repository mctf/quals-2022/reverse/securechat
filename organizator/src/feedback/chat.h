#pragma once

#include "stdafx.h"
#include <ObjIdl.h>
#include "Definition.h"

#pragma comment(lib, "gdiplus.lib")
using namespace Gdiplus;


void CreateChat(HWND hParent, HINSTANCE hInst, HFONT hFont, const WCHAR * username);
LRESULT CALLBACK ChatWndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

