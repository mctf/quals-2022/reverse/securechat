#pragma once

enum MessageType {
	CHAT = 1,
	END_CHAT,
	LOGIN,
	LOGIN_SUCCESS,
	LOGIN_FAIL
};

struct Message {
	MessageType code;
	WCHAR sender[128];
	WCHAR content[8192];
};

#define MAX_LOADSTRING 100
#define ARRAY_LENGTH(x) (sizeof(x) / sizeof((x)[0]))