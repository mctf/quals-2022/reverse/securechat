#define IDC_MYICON                      2
#define ID_CHATBOX                      9
#define IDD_CLIENT_DIALOG               102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_CLIENT                      107
#define IDC_CLIENT                      109
#define IDC_LOGIN                       110
#define IDC_SEND                        112
#define IDC_RECEIVE                     113
#define IDC_CREATE                      116
#define IDC_ADD                         117
#define IDC_ATTACH                      119
#define IDR_MAINFRAME                   128
#define IDI_SMALL                       131
#define IDC_STATIC                      -1

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           120
#endif
#endif
