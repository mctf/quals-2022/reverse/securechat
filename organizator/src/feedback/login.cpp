#include "stdafx.h"
#include "Definition.h"
#include "login.h"
#include "chat.h"


HINSTANCE hInst;

HWND hLogIn;
HWND hUsername;
HWND hPassword;

HWND hCreate;
HWND _hwnd;
HFONT hFont;
GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR gdiplusToken;


BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem);
void OnPaint(HWND hWnd);


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DRAWITEM, OnDrawItem);
	case WM_DESTROY: {
		GdiplusShutdown(gdiplusToken);
		PostQuitMessage(0);
		break;
	}
	case WM_CTLCOLORSTATIC: {
		HWND hStatic = (HWND)lParam;
		HDC hdc = (HDC)wParam;

		SetBkMode(hdc, TRANSPARENT);
		return (LRESULT)GetStockObject(DC_BRUSH);
	}
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct) {
	INITCOMMONCONTROLSEX icex;
	icex.dwSize = sizeof(icex);
	icex.dwICC = ICC_DATE_CLASSES;

	InitCommonControlsEx(&icex);
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	hFont = CreateFont(17, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE, L"Arial");

	if (hFont == NULL) {
		LOGFONT lf;
		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
		CreateFont(lf.lfHeight * 1.6, lf.lfWidth * 1.6,
			lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
			lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);
	}

	SetWindowFont(hWnd, hFont, true);
	hUsername = CreateWindowEx(0, L"edit", L"", WS_VISIBLE | WS_CHILD, 50, 160, 280, 35, hWnd, 0, hInst, 0);
	SetWindowFont(hUsername, hFont, true);
	hPassword = CreateWindowEx(0, L"edit", L"", WS_VISIBLE | WS_CHILD | ES_PASSWORD, 50, 250, 280, 35, hWnd, (HMENU)99, hInst, 0);
	SetWindowFont(hPassword, hFont, true);
	hLogIn = CreateWindowEx(0, L"button", L"Log In", WS_VISIBLE | WS_CHILD | BS_OWNERDRAW, 50, 320, 280, 30, hWnd, (HMENU)IDC_LOGIN, hInst, 0);

	return true;
}

void OnCommand(HWND hWnd, int lParam, HWND wParam, UINT codeNotify) {
	switch (lParam) {
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	case IDC_RECEIVE: {
		Message* message = (Message*)wParam;

		switch (message->code) {
		case MessageType::LOGIN_FAIL: {
			MessageBox(hWnd, L"Invalid credentials.", 0, 0);
			break;
		}
		case MessageType::LOGIN_SUCCESS: {
			InvalidateRect(hWnd, 0, TRUE);

			CreateChat(hWnd, hInst, hFont, message->sender);
			break;
		}
		}
		break;
	}
	case IDC_LOGIN: {
		WCHAR username[50];
		WCHAR password[50];
		Message* message = new Message{ MessageType::LOGIN, 0, 0 };

		GetWindowText(hUsername, username, ARRAY_LENGTH(username));
		if (username[0] == NULL) {
			MessageBox(hWnd, L"Username is empty.", 0, 0);
			return;
		}
		wcscpy_s(message->sender, ARRAY_LENGTH(message->sender), username);

		GetWindowText(hPassword, password, ARRAY_LENGTH(password));
		if (password[0] == NULL) {
			MessageBox(hWnd, L"Password is empty.", 0, 0);
			return;
		}
		wcscpy_s(message->content, ARRAY_LENGTH(message->content), password);
		Connection::instance().send(message);
		ShowWindow(hWnd, SW_HIDE);

		break;
	}
	}
}

void OnDrawItem(HWND hwnd, const DRAWITEMSTRUCT* lpDrawItem) {
	if (lpDrawItem->CtlID == IDC_LOGIN) {
		auto graphics = new Graphics(lpDrawItem->hDC);
		Gdiplus::SolidBrush brush(Gdiplus::Color(255, 69, 215, 194));
		graphics->FillRectangle(&brush, 0, 0, 872, 30);

		Gdiplus::FontFamily  fontFamily(L"Arial");
		Gdiplus::Font        font(&fontFamily, 15, Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
		Gdiplus::PointF      pointF(120, 6);
		Gdiplus::SolidBrush  solidBrush(Gdiplus::Color(255, 255, 255, 255));
		graphics->DrawString(L"Log In", -1, &font, pointF, &solidBrush);

		if (graphics)
			delete graphics;
	}
}

void OnPaint(HWND hWnd) {
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);

	auto graphics = new Graphics(hdc);
	Gdiplus::Pen pen(Gdiplus::Color(255, 37, 156, 236));
	graphics->DrawRectangle(&pen, 49, 159, 282, 37);
	graphics->DrawRectangle(&pen, 49, 249, 282, 37);

	Gdiplus::FontFamily  fontFamily(L"Arial");
	Gdiplus::Font        font(&fontFamily, 15, Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
	Gdiplus::PointF      pointF(46.0f, 135.0f);
	Gdiplus::SolidBrush  solidBrush(Gdiplus::Color(255, 37, 156, 236));

	graphics->DrawString(L"Username:", -1, &font, pointF, &solidBrush);

	pointF = Gdiplus::PointF(46.0f, 225.0f);
	graphics->DrawString(L"Password:", -1, &font, pointF, &solidBrush);

	EndPaint(hWnd, &ps);
}
