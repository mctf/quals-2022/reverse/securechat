#include "stdafx.h"
#include "Definition.h"
#include "main.h"
#include "chat.h"
#include "connection.h"

#define MAX_LOADSTRING 100

WCHAR szTitle[MAX_LOADSTRING];
WCHAR szWindowClass[MAX_LOADSTRING];

HWND                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);


bool registerWndClass(WNDPROC lpfnWndProc, WCHAR* szClassName, HINSTANCE hInst) {
	WNDCLASSEX wincl;

	wincl.hInstance = hInst;
	wincl.lpszClassName = szClassName;
	wincl.lpfnWndProc = lpfnWndProc;
	wincl.style = CS_HREDRAW | CS_VREDRAW;
	wincl.cbSize = sizeof(WNDCLASSEX);

	wincl.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_CLIENT));
	wincl.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wincl.hCursor = LoadCursor(NULL, IDC_ARROW);
	wincl.hbrBackground = (HBRUSH)(COLOR_BTNHILIGHT + 1);
	wincl.lpszMenuName = NULL;
	wincl.cbClsExtra = 0;
	wincl.cbWndExtra = 0;

	return RegisterClassEx(&wincl);
}

int APIENTRY wWinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/, LPWSTR /*lpCmdLine*/, int nCmdShow) {
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, ARRAY_LENGTH(szTitle));
	LoadStringW(hInstance, IDC_CLIENT, szWindowClass, ARRAY_LENGTH(szWindowClass));


	if (registerWndClass(WndProc, szWindowClass, hInstance) == false)
		return 0;

	if (registerWndClass(ChatWndProc, (WCHAR *)L"chatbox", hInstance) == false)
		return 0;


	HWND hWnd = InitInstance(hInstance, nCmdShow);
	if (!hWnd)
		return FALSE;

	Connection::instance().init(hWnd, "192.168.43.115", 8084);
	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_CLIENT));
	MSG msg;

	while (GetMessage(&msg, nullptr, 0, 0)) {
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}

HWND InitInstance(HINSTANCE hInstance, int nCmdShow) {
	HWND hWnd = CreateWindowW(
		szWindowClass,
		L"Security Inc Chat",
		WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
		CW_USEDEFAULT,
		0,
		400,
		600,
		nullptr,
		nullptr,
		hInstance,
		nullptr
	);

	if (!hWnd)
		return 0;

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return hWnd;
}
