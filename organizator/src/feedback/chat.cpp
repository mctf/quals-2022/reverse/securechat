#include "stdafx.h"
#include "resource.h"
#include "chat.h"
#include "Connection.h"


HINSTANCE _hInst;
HWND _hTextBox;
HWND _hMessageBox;
HWND _hWindow;
HWND _hSend;
HWND _hParent;
HWND _hWnd;
HFONT _hFont;

std::wstring _username;


void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void setChatActive();
void onRecv(const WCHAR* sender, const WCHAR* content);
int onSendButtonClick(Message* message);
void onUserLeft(WCHAR* user);
void onUserJoin(WCHAR* user);


void CreateChat(HWND hParent, HINSTANCE hInst, HFONT hFont, const WCHAR* username) {
	_hWnd = CreateWindowEx(0, L"chatbox", L"Chat", WS_CAPTION | WS_SYSMENU | WS_VISIBLE | WS_MINIMIZEBOX, 100, 200, 500, 500, NULL, NULL, hInst, NULL);
	Connection::instance().setHwnd(_hWnd);
	_hInst = hInst;
	_hParent = hParent;
	_hMessageBox = CreateWindowEx(WS_EX_RIGHTSCROLLBAR, L"edit", L"", WS_VISIBLE | WS_VSCROLL | WS_CHILD | ES_AUTOVSCROLL | ES_MULTILINE | ES_READONLY | WS_BORDER, 500 * 0.025, 500* 0.025, 500 * 0.925, 500* 0.635, _hWnd, (HMENU)1000, hInst, 0);
	_hTextBox = CreateWindow(L"edit", L"", WS_VISIBLE | WS_CHILD | ES_MULTILINE | WS_BORDER, 500 * 0.025, 500* 0.7, 500 * 0.75, 500 * 0.15, _hWnd, 0, hInst, 0);
	_hSend = CreateWindow(L"button", L"Send", WS_VISIBLE | WS_CHILD | BS_OWNERDRAW, 500 * 0.8, 500* 0.7, 500 * 0.15, 500* 0.15, _hWnd, (HMENU)IDC_SEND, hInst, 0);
	_username = std::wstring(username);

	SetWindowFont(_hMessageBox, hFont, true);
	SetWindowFont(_hTextBox, hFont, true);
}


LRESULT CALLBACK ChatWndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_CTLCOLORSTATIC: {
		HWND hStatic = (HWND)lParam;
		HDC hdc = (HDC)wParam;

		SetBkMode(hdc, TRANSPARENT);
		return (LRESULT)GetStockObject(DC_BRUSH);
	}
	case WM_DRAWITEM: {
		auto dit = (DRAWITEMSTRUCT*)(lParam);
		switch (dit->CtlID) {
		case IDC_SEND: {
			auto graphics = new Graphics(dit->hDC);
			Gdiplus::SolidBrush brush(Gdiplus::Color(255, 69, 215, 194));
			graphics->FillRectangle(&brush, 0, 0, 75, 75);

			Gdiplus::FontFamily  fontFamily(L"Arial");
			Gdiplus::Font        font(&fontFamily, 17, Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
			Gdiplus::PointF      pointF(15, 30);
			Gdiplus::SolidBrush  solidBrush(Gdiplus::Color(255, 255, 255, 255));

			graphics->DrawString(L"Send", -1, &font, pointF, &solidBrush);

			if (graphics)
				delete graphics;
			break;
		}
		}
		break;
	}
	case WM_DESTROY: {
		Message* message = new Message{ MessageType::END_CHAT, 0, 0 };
		wcscpy_s(message->sender, ARRAY_LENGTH(message->sender), _username.c_str());
		Connection::instance().send(message);
		delete message;
		DestroyWindow(_hParent);
		DestroyWindow(hwnd);
		ExitProcess(0);
		break;
	}
	case WM_COMMAND:
		switch (LOWORD(wParam)) {
		case IDC_SEND: {
			Message* message = new Message;
			if (onSendButtonClick(message) == -1) {
				return 0;
			}

			Connection::instance().send(message);
			delete message;
			break;
		}
		case IDM_EXIT:
			DestroyWindow(hwnd);
			break;
		case IDC_RECEIVE: {
			Message* message = (Message*)lParam;

			switch (message->code) {
			case MessageType::CHAT: {
				onRecv(message->sender, message->content);
				break;
			}
			case MessageType::END_CHAT: {
				onUserLeft(message->sender);
				break;
			}

			}
			break;
		}
		}
		break;
	}

	return DefWindowProc(hwnd, message, wParam, lParam);
}

void setChatActive() {
	SendMessageA(_hMessageBox, EM_SETSEL, 0, -1);
	SendMessageA(_hMessageBox, EM_SETSEL, -1, -1);
	SendMessageA(_hMessageBox, EM_SCROLLCARET, 0, 0);
}

void onRecv(const WCHAR* sender, const WCHAR* content) {
	WCHAR buffer[10000];
	GetWindowText(_hMessageBox, buffer, sizeof(buffer) / 2);

	wcscat(buffer, L"\r\n[");
	wcscat(buffer, sender);
	wcscat(buffer, L"]: ");
	wcscat(buffer, content);
	SetWindowText(_hMessageBox, buffer);
	setChatActive();
}

int onSendButtonClick(Message * message) {
	WCHAR bufferText[1000];
	WCHAR bufferMess[10000];
	message->code = MessageType::CHAT;
	GetWindowText(_hTextBox, bufferText, 1000);
	SetWindowText(_hTextBox, L"");
	if (bufferText[0] == NULL) {
		return -1;
	}

	wcscpy(message->sender, _username.c_str());
	wcscpy(message->content, bufferText);

	GetWindowText(_hMessageBox, bufferMess, 10000);
	wcscat(bufferMess, L"\r\n[You]: ");
	wcscat(bufferMess, bufferText);
	SetWindowText(_hMessageBox, bufferMess);
	setChatActive();

	return wcsnlen_s(bufferMess, sizeof(bufferMess));
}

void onUserLeft(WCHAR* user) {
	WCHAR buffer[10000];
	GetWindowText(_hMessageBox, buffer, 10000);
	wsprintfW(buffer, L"%s\r\n%s has left.\r\n", buffer, user);
	SetWindowText(_hMessageBox, buffer);
	setChatActive();
}

void onUserJoin(WCHAR* user) {
	WCHAR buffer[10000];
	GetWindowText(_hMessageBox, buffer, 10000);
	wsprintfW(buffer, L"%s\r\n%s has joined.\r\n", buffer, user);
	SetWindowText(_hMessageBox, buffer);
	setChatActive();
}
