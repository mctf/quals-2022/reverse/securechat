FROM ubuntu:latest

RUN apt-get -y update && apt-get -y install lighttpd
RUN rm -f /var/www/html/*
WORKDIR /var/www/html/

COPY ./participant.tar.gz .

EXPOSE 8080

CMD ["lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf"]